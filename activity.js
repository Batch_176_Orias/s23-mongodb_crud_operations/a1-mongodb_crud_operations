db.users1.insertMany([
		{
			firstName: "John",
			lastName: "Lennon",
			email: "jlennon1940@gmail.com",
			password: "imagine80",
			isAdmin: false
		},
		{
			firstName: "Paul",
			lastName: "McCartney",
			email: "paulisAlive@gmail.com",
			password: "paulYesterdat64",
			isAdmin: false
		},
		{
			firstName: "George",
			lastName: "Harrison",
			email: "somethingGeorge@gmail.com",
			password: "iAmTheSun",
			isAdmin: false
		},
		{
			firstName: "Richard",
			lastName: "Starsky",
			email: "ringoStarr@gmail.com",
			password: "yellowSubmarine",
			isAdmin: false
		},
		{
			firstName: "Pete",
			lastName: "Best",
			email: "pbBeatles@gmail.com",
			password: "peteTheBest",
			isAdmin: false
		}
	])

db.courses1.insertMany([
		{
			name: "course1",
			price: 2000,
			isActive: false
		},
		{
			name: "course2",
			price: 3000,
			isActive: false
		},
		{
			name: "course3",
			price: 4000,
			isActive: false
		}
	])

db.users1.find({
	isAdmin: false
})

db.users1.updateOne(
		{
			firstName: "John"
		},
		{
			$set: {
				isAdmin: true
			}
		}
	)

db.courses1.updateOne(
		{
			name: "course2"
		},
		{
			$set: {
				isActive: true
			}
		}
	)

db.courses1.deleteMany(
		{
			isActive: false
		}
	)